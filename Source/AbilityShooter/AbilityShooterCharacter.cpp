// Copyright Epic Games, Inc. All Rights Reserved.

#include "AbilityShooterCharacter.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameplayEffectExtension.h"
#include "AbilitySystem/AS_AbilitySystemComponent.h"
#include "AbilitySystem/AS_AttributeSetParent.h"
#include "DataAssets/AS_CharacterDataAsset.h"
#include "Net/UnrealNetwork.h"
#include "Actor/AS_CharacterMovementComponent.h"
#include "Inventory/InventoryComponent.h"


//////////////////////////////////////////////////////////////////////////
// AAbilityShooterCharacter

AAbilityShooterCharacter::AAbilityShooterCharacter(const FObjectInitializer& ObjectInitializer) :
Super(ObjectInitializer.SetDefaultSubobjectClass<UAS_CharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f
	;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)

	AbilitySystemComponent = CreateDefaultSubobject<UAS_AbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxMovementSpeedAttribute()).AddUObject(this, &AAbilityShooterCharacter::OnMaxMovementSpeedChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(this, &AAbilityShooterCharacter::OnHeathChanged);

	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(TEXT("State.Death")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AAbilityShooterCharacter::OnDeathStateChanged);

	AttributeSet = CreateDefaultSubobject<UAS_AttributeSetParent>(TEXT("AttributeSet"));

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	InventoryComponent->SetIsReplicated(true);
}

bool AAbilityShooterCharacter::ApplyGameplayEffectToSelf(TSubclassOf<UGameplayEffect> Effect,
	FGameplayEffectContextHandle InEffectContext)
{
	if (!Effect.Get()) return false;

	const FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(Effect, 1, InEffectContext);
	if (SpecHandle.IsValid())
	{
		const FActiveGameplayEffectHandle ActiveGameplayEffectHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		return ActiveGameplayEffectHandle.WasSuccessfullyApplied();
	}
	return false;
}

UAbilitySystemComponent* AAbilityShooterCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void AAbilityShooterCharacter::PostLoad()
{
	Super::PostLoad();

	if (IsValid(CharacterDataAsset))
	{
		SetCharacterData(CharacterDataAsset->CharacterData);
	}
}

void AAbilityShooterCharacter::OnJumpActionStarted(const FInputActionValue& Value)
{
	FGameplayEventData Payload;

	Payload.Instigator = this;
	Payload.EventTag = JumpEventTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, JumpEventTag, Payload);
}

void AAbilityShooterCharacter::OnJumpActionEnded(const FInputActionValue& Value)
{
	
}

void AAbilityShooterCharacter::OnCrouchActionStarted(const FInputActionValue& Value)
{
	AbilitySystemComponent->TryActivateAbilitiesByTag(CrouchAbilityTag, true);
}

void AAbilityShooterCharacter::OnCrouchActionEnded(const FInputActionValue& Value)
{
	AbilitySystemComponent->CancelAbilities(&CrouchAbilityTag);
}

void AAbilityShooterCharacter::OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnStartCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	ApplyGameplayEffect(CrouchGameplayEffect);
}

void AAbilityShooterCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(CrouchGameplayEffect, AbilitySystemComponent);
	}
}

void AAbilityShooterCharacter::OnSprintActionStarted(const FInputActionValue& Value)
{
	AbilitySystemComponent->TryActivateAbilitiesByTag(SprintAbilityTag, true);
}

void AAbilityShooterCharacter::OnSprintActionEnded(const FInputActionValue& Value)
{
	AbilitySystemComponent->CancelAbilities(&SprintAbilityTag);
}

void AAbilityShooterCharacter::OnEquipItemAction(const FInputActionValue& Value)
{
	FGameplayEventData GameplayEventData;
	GameplayEventData.EventTag = UInventoryComponent::EquipNextTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, UInventoryComponent::EquipNextTag, GameplayEventData);
}

void AAbilityShooterCharacter::OnDropItemAction(const FInputActionValue& Value)
{
	FGameplayEventData GameplayEventData;
	GameplayEventData.EventTag = UInventoryComponent::DropItemTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, UInventoryComponent::DropItemTag, GameplayEventData);
}

void AAbilityShooterCharacter::OnUnequipItemAction(const FInputActionValue& Value)
{
	FGameplayEventData GameplayEventData;
	GameplayEventData.EventTag = UInventoryComponent::UnequipTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, UInventoryComponent::UnequipTag, GameplayEventData);
}

void AAbilityShooterCharacter::OnAttackActionStarted(const FInputActionValue& Value)
{
	FGameplayEventData GameplayEventData;
	GameplayEventData.EventTag = AttackStartedEventTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, AttackStartedEventTag, GameplayEventData);
}

void AAbilityShooterCharacter::OnAttackActionEnded(const FInputActionValue& Value)
{
	FGameplayEventData GameplayEventData;
	GameplayEventData.EventTag = AttackEndedEventTag;

	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, AttackEndedEventTag, GameplayEventData);
}

void AAbilityShooterCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);

	AbilitySystemComponent->RemoveActiveEffectsWithTags(InAirTags);
}

void AAbilityShooterCharacter::GiveAbilities()
{
	if (HasAuthority() && AbilitySystemComponent)
	{
		for (const auto DefaultAbility : CharacterData.Abilities)
		{
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(DefaultAbility));
		}
	}
}

void AAbilityShooterCharacter::ApplyStartupEffects()
{
	if (HasAuthority())
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);
		
		for (auto const DefaultEffect : CharacterData.Effects)
		{
			ApplyGameplayEffectToSelf(DefaultEffect, EffectContext);
		}
	}
}

void AAbilityShooterCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AbilitySystemComponent->InitAbilityActorInfo(this, this);
	
	GiveAbilities();
	ApplyStartupEffects();
}

void AAbilityShooterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	AbilitySystemComponent->InitAbilityActorInfo(this, this);
}

void AAbilityShooterCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void AAbilityShooterCharacter::OnRep_CharacterData()
{
	InitFromCharacterData(CharacterData, true);
}

void AAbilityShooterCharacter::InitFromCharacterData(const FCharacterData& InCharacterData, bool bFromReplication)
{
	
}

FCharacterData AAbilityShooterCharacter::GetCharacterData() const
{
	return CharacterData;
}

void AAbilityShooterCharacter::SetCharacterData(const FCharacterData& InCharacterData)
{
	CharacterData = InCharacterData;

	InitFromCharacterData(CharacterData, false);
}

FActiveGameplayEffectHandle AAbilityShooterCharacter::ApplyGameplayEffect(
	const TSubclassOf<UGameplayEffect> GameplayEffectToApply)
{
	if (GameplayEffectToApply.Get() && AbilitySystemComponent)
	{
		const FGameplayEffectContextHandle GEContextHandle = AbilitySystemComponent->MakeEffectContext();

		const FGameplayEffectSpecHandle GESpecHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffectToApply, 1, GEContextHandle);

		if (GESpecHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*GESpecHandle.Data.Get(), AbilitySystemComponent);

			if (!ActiveGEHandle.WasSuccessfullyApplied())
			{
				UE_LOG(LogTemp, Error, TEXT("Ability %s failed to apply effect %s"), *GetName(), *GetNameSafe(GameplayEffectToApply));
			}
			else
			{
				return ActiveGEHandle;
			}
		}
	}
	
	return {};
}

void AAbilityShooterCharacter::OnMaxMovementSpeedChanged(const FOnAttributeChangeData& Data)
{
	GetCharacterMovement()->MaxWalkSpeed = Data.NewValue;
}

void AAbilityShooterCharacter::OnHeathChanged(const FOnAttributeChangeData& Data)
{
	if (Data.NewValue <= 0 && Data.OldValue >= 0)
	{
		if (Data.GEModData)
		{
			AAbilityShooterCharacter* Killer = nullptr;
			const FGameplayEffectContextHandle& EffectContextHandle = Data.GEModData->EffectSpec.GetEffectContext();
			Killer = Cast<AAbilityShooterCharacter>(EffectContextHandle.GetInstigator());
		}

		FGameplayEventData EventData;
		EventData.EventTag = DeathEventTag;

		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, DeathEventTag, EventData);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAbilityShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &AAbilityShooterCharacter::OnJumpActionStarted);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &AAbilityShooterCharacter::OnJumpActionEnded);
		
		//Sprinting
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &AAbilityShooterCharacter::OnSprintActionStarted);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &AAbilityShooterCharacter::OnSprintActionEnded);

		//Crouching
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Triggered, this, &AAbilityShooterCharacter::OnCrouchActionStarted);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Completed, this, &AAbilityShooterCharacter::OnCrouchActionEnded);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AAbilityShooterCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AAbilityShooterCharacter::Look);

		EnhancedInputComponent->BindAction(EquipItemAction, ETriggerEvent::Started, this, &AAbilityShooterCharacter::OnEquipItemAction);
		EnhancedInputComponent->BindAction(DropItemAction, ETriggerEvent::Started, this, &AAbilityShooterCharacter::OnDropItemAction);
		EnhancedInputComponent->BindAction(UnequipItemAction, ETriggerEvent::Started, this, &AAbilityShooterCharacter::OnUnequipItemAction);

		//Attacking
		EnhancedInputComponent->BindAction(AttackInputAction, ETriggerEvent::Started, this, &AAbilityShooterCharacter::OnAttackActionStarted);
		EnhancedInputComponent->BindAction(AttackInputAction, ETriggerEvent::Completed, this, &AAbilityShooterCharacter::OnAttackActionEnded);
	}

}

void AAbilityShooterCharacter::OnDeathStateChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	
}

void AAbilityShooterCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AAbilityShooterCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AAbilityShooterCharacter::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AAbilityShooterCharacter, CharacterData);
	DOREPLIFETIME(AAbilityShooterCharacter, InventoryComponent);
}




