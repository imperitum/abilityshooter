// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AS_CharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_CharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
};
