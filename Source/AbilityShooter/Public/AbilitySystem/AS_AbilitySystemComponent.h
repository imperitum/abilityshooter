// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AS_AbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_AbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
};
