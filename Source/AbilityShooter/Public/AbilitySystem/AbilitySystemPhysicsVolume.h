// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PhysicsVolume.h"
#include "GameplayAbilitySpec.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemPhysicsVolume.generated.h"

struct FGameplayTag;
class UGameplayAbility;
class UGameplayEffect;


USTRUCT(BlueprintType)
struct FAbilityVolumeEnteredActors
{
	GENERATED_BODY()

	TArray<FGameplayAbilitySpecHandle> AppliedAbilities;

	TArray<FActiveGameplayEffectHandle> AppliedEffects;
};

UCLASS()
class ABILITYSHOOTER_API AAbilitySystemPhysicsVolume : public APhysicsVolume
{
	GENERATED_BODY()

public:

	AAbilitySystemPhysicsVolume();

	virtual void ActorEnteredVolume(AActor* Other) override;

	virtual void ActorLeavingVolume(AActor* Other) override;

	virtual void Tick(float DeltaSeconds) override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayEffect>> OngoingEffectsToApply;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayAbility>> OngoingAbilitiesToGive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayAbility>> PermanentAbilitiesToGive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayEffect>> OnExitEffectsToApply;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FGameplayTag> EventsToSendOnEnter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FGameplayTag> EventsToSendOnExit;

	TMap<AActor*, FAbilityVolumeEnteredActors> EnteredActorsInfoMap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDrawDebug = false;
	
};
