// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/AS_ParentGameplayAbility.h"
#include "AS_InventoryAbility.generated.h"

class AWeaponItemActor;
class UAS_InventoryItemInstance;
/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_InventoryAbility : public UAS_ParentGameplayAbility
{
	GENERATED_BODY()

public:

	virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	UFUNCTION(BlueprintPure)
	UInventoryComponent* GetInventoryComponent() const;

	UFUNCTION(BlueprintPure)
	const UAS_InventoryItemInstance* GetEquippedItemInstance() const;

	UFUNCTION(BlueprintPure)
	const UItemStaticData* GetEquippedItemStaticData() const;

	UFUNCTION(BlueprintPure)
	const UWeaponStaticData* GetEquippedWeaponStaticData() const;
	
	UFUNCTION(BlueprintPure)
	AItemActor* GetEquippedItemActor() const;

	UFUNCTION(BlueprintPure)
	AWeaponItemActor* GetEquippedWeaponActor() const;

protected:

	UPROPERTY()
	UInventoryComponent* InventoryComponent = nullptr;
};
