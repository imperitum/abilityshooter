// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/AS_InventoryAbility.h"
#include "AS_AttackInventoryAbility.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_AttackInventoryAbility : public UAS_InventoryAbility
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	FGameplayEffectSpecHandle GetWeaponEffectSpec(const FHitResult& Hit);
	
};
