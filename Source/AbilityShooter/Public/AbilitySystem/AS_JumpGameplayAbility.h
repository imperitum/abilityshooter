// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/AS_ParentGameplayAbility.h"
#include "AS_JumpGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_JumpGameplayAbility : public UAS_ParentGameplayAbility
{
	GENERATED_BODY()

	UAS_JumpGameplayAbility();

public:

	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
};
