// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityShooterCharacter.h"
#include "Abilities/GameplayAbility.h"
#include "AS_ParentGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_ParentGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	FActiveGameplayEffectHandle ApplyGameplayEffect(const FGameplayAbilityActorInfo* ActorInfo, const TSubclassOf<UGameplayEffect> GameplayEffectToApply);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AAbilityShooterCharacter* GetAbilityShooterCharacterFromActorInfo() const;

protected:

	UPROPERTY(EditDefaultsOnly, Category="Effects")
	TArray<TSubclassOf<UGameplayEffect>> EffectsToApplyOnStart;

	UPROPERTY(EditDefaultsOnly, Category="Effects")
	TArray<TSubclassOf<UGameplayEffect>> EffectsToRemoveOnEnd;

	TArray<FActiveGameplayEffectHandle> EffectsHandlesToRemove;
	
};
