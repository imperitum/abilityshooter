// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AS_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ABILITYSHOOTER_API UAS_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	virtual void Init() override;
	
};
