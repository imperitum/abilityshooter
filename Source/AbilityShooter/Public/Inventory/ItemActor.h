// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Actor.h"
#include "AS_DataTypes.h"
#include "ItemActor.generated.h"

class USphereComponent;
class UAS_InventoryItemInstance;

UCLASS()
class ABILITYSHOOTER_API AItemActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemActor();
	
	virtual void OnEquipped();
	virtual void OnUnequipped();
	virtual void OnDropped();

	void Init(UAS_InventoryItemInstance* InInstance);

	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

	UPROPERTY(ReplicatedUsing=OnRep_ItemState)
	TEnumAsByte<EItemState> ItemState = EItemState::None;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void InitInternal();

	UFUNCTION()
	void OnRep_ItemState();

	UPROPERTY(ReplicatedUsing=OnRep_ItemInstance)
	UAS_InventoryItemInstance* ItemInstance = nullptr;

	UFUNCTION()
	void OnRep_ItemInstance(UAS_InventoryItemInstance* OldItemInstance);
	
	UPROPERTY()
	USphereComponent* SphereComponent = nullptr;

	UFUNCTION()
	void OnActorOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UPROPERTY(EditAnywhere)
	TSubclassOf<UItemStaticData> ItemStaticDataClass;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
