// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AS_DataTypes.h"
#include "GameplayAbilitySpec.h"
#include "AS_InventoryItemInstance.generated.h"

UCLASS()
class ABILITYSHOOTER_API UAS_InventoryItemInstance : public UObject
{
	GENERATED_BODY()

public:

	virtual void Init(TSubclassOf<UItemStaticData> InItemStaticDataClass);

	virtual bool IsSupportedForNetworking() const override {return true;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const UItemStaticData* GetItemStaticData() const;

	UPROPERTY(Replicated)
	TSubclassOf<UItemStaticData> ItemStaticDataClass;

	UPROPERTY(ReplicatedUsing=OnRep_IsEquipped)
	bool bIsEquipped = false;

	UFUNCTION()
	void OnRep_IsEquipped();

	virtual void OnEquipped(AActor* InOwner = nullptr);
	virtual void OnUnequipped(AActor* InOwner = nullptr);
	virtual void OnDropped(AActor* InOwner = nullptr);

	UFUNCTION(BlueprintPure)
	AItemActor* GetItemActor() const;

protected:

	UPROPERTY(Replicated)
	AItemActor* ItemActor = nullptr;

	void TryToGrantAbilities(AActor* InOwner = nullptr);

	void TryToRemoveAbilities(AActor* InOwner = nullptr);

	UPROPERTY()
	TArray<FGameplayAbilitySpecHandle> GrantedAbilityHandles;
};
