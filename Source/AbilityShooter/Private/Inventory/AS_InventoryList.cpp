// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/AS_InventoryList.h"

#include "Inventory/AS_InventoryItemInstance.h"

void FInventoryList::AddItem(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	FInventoryListItem& Item = Items.AddDefaulted_GetRef();
	Item.InventoryItemInstance = NewObject<UAS_InventoryItemInstance>();
	Item.InventoryItemInstance->Init(InItemStaticDataClass);
	MarkItemDirty(Item);
}

void FInventoryList::AddItem(UAS_InventoryItemInstance* InItemInstance)
{
	FInventoryListItem& Item = Items.AddDefaulted_GetRef();
	Item.InventoryItemInstance = InItemInstance;
	MarkItemDirty(Item);
}

void FInventoryList::RemoveItem(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	for (auto ItemIter = Items.CreateIterator(); ItemIter; ++ItemIter)
	{
		const auto Item = *ItemIter;
		if (Item.InventoryItemInstance && Item.InventoryItemInstance->GetItemStaticData()->IsA(InItemStaticDataClass))
		{
			ItemIter.RemoveCurrent();
			MarkArrayDirty();
			break;
		}
	}
}
