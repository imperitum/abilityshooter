// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/AS_InventoryItemInstance.h"

#include "AbilityShooterStatics.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "GameFramework/Character.h"
#include "Inventory/ItemActor.h"
#include "Net/UnrealNetwork.h"

void UAS_InventoryItemInstance::Init(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	ItemStaticDataClass = InItemStaticDataClass;
}

const UItemStaticData* UAS_InventoryItemInstance::GetItemStaticData() const
{
	return UAbilityShooterStatics::GetItemStaticData(ItemStaticDataClass);
}

void UAS_InventoryItemInstance::OnRep_IsEquipped()
{
	
}

void UAS_InventoryItemInstance::OnEquipped(AActor* InOwner)
{
	if (UWorld* World = InOwner->GetWorld())
	{
		const UItemStaticData* ItemStaticData = GetItemStaticData();
		
		const FTransform Transform;
		ItemActor = World->SpawnActorDeferred<AItemActor>(GetItemStaticData()->ItemActorClass, Transform, InOwner);
		ItemActor->Init(this);
		ItemActor->OnEquipped();

		ItemActor->FinishSpawning(Transform);

		const ACharacter* Character = Cast<ACharacter>(InOwner);
		if (USkeletalMeshComponent* SkeletalMesh = Character ? Character->GetMesh() : nullptr)
		{
			ItemActor->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, ItemStaticData->AttachmentSocketName);
		}
	}

	TryToGrantAbilities(InOwner);

	bIsEquipped = true;
}

void UAS_InventoryItemInstance::OnUnequipped(AActor* InOwner)
{
	if (ItemActor)
	{
		ItemActor->Destroy();
		ItemActor = nullptr;
	}

	TryToRemoveAbilities(InOwner);

	bIsEquipped = false;
}

void UAS_InventoryItemInstance::OnDropped(AActor* InOwner)
{
	if (ItemActor)
	{
		ItemActor->OnDropped();
	}

	TryToRemoveAbilities(InOwner);
	
	bIsEquipped = false;
}

AItemActor* UAS_InventoryItemInstance::GetItemActor() const
{
	return ItemActor;
}

void UAS_InventoryItemInstance::TryToGrantAbilities(AActor* InOwner)
{
	if (InOwner && InOwner->HasAuthority())
	{
		if (UAbilitySystemComponent* AbilityComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(InOwner))
		{
			const UItemStaticData* ItemStaticData = GetItemStaticData();

			for (auto Ability : ItemStaticData->GrantedAbilities)
			{
				GrantedAbilityHandles.Add(AbilityComponent->GiveAbility(FGameplayAbilitySpec(Ability)));
			}
		}
	}
}

void UAS_InventoryItemInstance::TryToRemoveAbilities(AActor* InOwner)
{
	if (InOwner && InOwner->HasAuthority())
	{
		if (UAbilitySystemComponent* AbilityComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(InOwner))
		{
			const UItemStaticData* ItemStaticData = GetItemStaticData();

			for (auto Ability : GrantedAbilityHandles)
			{
				AbilityComponent->ClearAbility(Ability);
			}
			GrantedAbilityHandles.Empty();
		}
	}
}

void UAS_InventoryItemInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UAS_InventoryItemInstance, ItemStaticDataClass);
	DOREPLIFETIME(UAS_InventoryItemInstance, bIsEquipped);
	DOREPLIFETIME(UAS_InventoryItemInstance, ItemActor);
}
