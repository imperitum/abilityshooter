// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/WeaponItemActor.h"

#include "Inventory/AS_InventoryItemInstance.h"

AWeaponItemActor::AWeaponItemActor()
{
	
}

const UWeaponStaticData* AWeaponItemActor::GetWeaponStaticData() const
{
	return ItemInstance ? Cast<UWeaponStaticData>(ItemInstance->GetItemStaticData()) : nullptr;
}

FVector AWeaponItemActor::GetMuzzleLocation() const
{
	return MeshComponent ? MeshComponent->GetSocketLocation(TEXT("Muzzle")) : GetActorLocation();
}

void AWeaponItemActor::InitInternal()
{
	Super::InitInternal();

	if (const UWeaponStaticData* WeaponStaticData = GetWeaponStaticData())
	{
		if (WeaponStaticData->SkeletalMesh)
		{
			if (USkeletalMeshComponent* SkeletalMesh = NewObject<USkeletalMeshComponent>(this, USkeletalMeshComponent::StaticClass(), TEXT("MeshComponent")))
			{
				SkeletalMesh->RegisterComponent();
				SkeletalMesh->SetSkeletalMesh(WeaponStaticData->SkeletalMesh);
				SkeletalMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
				MeshComponent = SkeletalMesh;
			}
		}
		else if (WeaponStaticData->StaticMesh)
		{
			if (UStaticMeshComponent* StaticMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), TEXT("MeshComponent")))
			{
				StaticMesh->RegisterComponent();
				StaticMesh->SetStaticMesh(WeaponStaticData->StaticMesh);
				StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
				MeshComponent = StaticMesh;
			}
		}
	}
}
