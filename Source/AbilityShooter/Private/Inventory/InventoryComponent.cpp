// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/InventoryComponent.h"

#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Inventory/AS_InventoryList.h"
#include "Inventory/AS_InventoryItemInstance.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayTagsManager.h"

FGameplayTag UInventoryComponent::EquipItemActorTag;
FGameplayTag UInventoryComponent::DropItemTag;
FGameplayTag UInventoryComponent::EquipNextTag;
FGameplayTag UInventoryComponent::UnequipTag;

static TAutoConsoleVariable<int32> CVarShowInventory(
TEXT("ShowDebugInventory"),
0,
TEXT("Draws info about inventory")
TEXT("0: off/n")
TEXT("1: on/n"),
ECVF_Cheat
);

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
	SetIsReplicatedByDefault(true);

	static bool bHandledAddingTags = false;
	if (!bHandledAddingTags)
	{
		bHandledAddingTags = true;
		UGameplayTagsManager::Get().OnLastChanceToAddNativeTags().AddUObject(this, &UInventoryComponent::AddInventoryTags);
	}
}

void UInventoryComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (GetOwner()->HasAuthority())
	{
		for (const auto Item : DefaultItems)
		{
			InventoryList.AddItem(Item);
		}

		// if (!InventoryList.GetItemsRef().IsEmpty())
		// {
		// 	EquipItem(InventoryList.GetItemsRef()[0].InventoryItemInstance->ItemStaticDataClass);
		// }
		//
		// DropItem();
	}

	if (UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner()))
	{
		ASC->GenericGameplayEventCallbacks.FindOrAdd(UInventoryComponent::EquipItemActorTag).AddUObject(this, &UInventoryComponent::GameplayEventCallback);
		ASC->GenericGameplayEventCallbacks.FindOrAdd(UInventoryComponent::EquipNextTag).AddUObject(this, &UInventoryComponent::GameplayEventCallback);
		ASC->GenericGameplayEventCallbacks.FindOrAdd(UInventoryComponent::DropItemTag).AddUObject(this, &UInventoryComponent::GameplayEventCallback);
		ASC->GenericGameplayEventCallbacks.FindOrAdd(UInventoryComponent::UnequipTag).AddUObject(this, &UInventoryComponent::GameplayEventCallback);
	}
}

bool UInventoryComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (auto Item : InventoryList.GetItemsRef())
	{
		if (UAS_InventoryItemInstance* ItemInstance = Item.InventoryItemInstance)
		{
			WroteSomething |= Channel->ReplicateSubobject(ItemInstance, *Bunch, *RepFlags);
		}
	}

	return WroteSomething;
}

void UInventoryComponent::AddItem(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	if (GetOwner()->HasAuthority())
	{
		InventoryList.AddItem(InItemStaticDataClass);
	}
}

void UInventoryComponent::AddItemInstance(UAS_InventoryItemInstance* InItemInstance)
{
	if (GetOwner()->HasAuthority())
	{
		InventoryList.AddItem(InItemInstance);
	}
}

void UInventoryComponent::RemoveItem(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	if (GetOwner()->HasAuthority())
	{
		InventoryList.RemoveItem(InItemStaticDataClass);
	}
}

void UInventoryComponent::EquipItem(TSubclassOf<UItemStaticData> InItemStaticDataClass)
{
	if (GetOwner()->HasAuthority())
	{
		for (const auto Item : InventoryList.GetItemsRef())
		{
			if (Item.InventoryItemInstance->ItemStaticDataClass == InItemStaticDataClass)
			{
				Item.InventoryItemInstance->OnEquipped(GetOwner());
				CurrentItem = Item.InventoryItemInstance;
				break;
			}
		}
	}
}

void UInventoryComponent::EquipItemInstance(UAS_InventoryItemInstance* InItemInstance)
{
	if (GetOwner()->HasAuthority())
	{
		for (const auto Item : InventoryList.GetItemsRef())
		{
			if (Item.InventoryItemInstance == InItemInstance)
			{
				Item.InventoryItemInstance->OnEquipped(GetOwner());
				CurrentItem = Item.InventoryItemInstance;
				break;
			}
		}
	}
}

void UInventoryComponent::EquipNext()
{
	auto Items = InventoryList.GetItemsRef();
	const bool bNoItems = Items.Num() == 0;
	const bool bOneAndEquipped = Items.Num() == 1 && CurrentItem;

	if (bNoItems || bOneAndEquipped) return;

	UAS_InventoryItemInstance* TargetItem = CurrentItem;

	for (const auto Item : Items)
	{
		if (Item.InventoryItemInstance->GetItemStaticData()->bCanBeEquipped && Item.InventoryItemInstance != CurrentItem)
		{
			TargetItem = Item.InventoryItemInstance;
			break;
		}
	}

	if (CurrentItem)
	{
		if (TargetItem == CurrentItem)
		{
			return;
		}

		UnequipItem();
	}

	EquipItemInstance(TargetItem);
}

void UInventoryComponent::UnequipItem()
{
	if (GetOwner()->HasAuthority())
	{
		if (CurrentItem)
		{
			CurrentItem->OnUnequipped(GetOwner());
			CurrentItem = nullptr;
		}
	}
}

void UInventoryComponent::DropItem()
{
	if (GetOwner()->HasAuthority())
	{
		if (CurrentItem)
		{
			CurrentItem->OnDropped(GetOwner());
			RemoveItem(CurrentItem->ItemStaticDataClass);
			CurrentItem = nullptr;
		}
	}
}

void UInventoryComponent::GameplayEventCallback(const FGameplayEventData* Payload)
{
	ENetRole Role = GetOwner()->GetLocalRole();

	switch (Role)
	{
	case ROLE_Authority: HandleGameplayEventInternal(*Payload);
		break;
	case ROLE_AutonomousProxy: ServerHandleGameplayEvent(*Payload);
		break;
	default: break;
	}
}

void UInventoryComponent::AddInventoryTags()
{
	UGameplayTagsManager& TagsManager = UGameplayTagsManager::Get();

	UInventoryComponent::EquipItemActorTag = TagsManager.AddNativeGameplayTag(TEXT("Event.Inventory.EquipItemActor"), TEXT(""));
	UInventoryComponent::DropItemTag = TagsManager.AddNativeGameplayTag(TEXT("Event.Inventory.DropItem"), TEXT(""));
	UInventoryComponent::EquipNextTag = TagsManager.AddNativeGameplayTag(TEXT("Event.Inventory.EquipNext"), TEXT(""));
	UInventoryComponent::UnequipTag = TagsManager.AddNativeGameplayTag(TEXT("Event.Inventory.Unequip"), TEXT(""));

	TagsManager.OnLastChanceToAddNativeTags().RemoveAll(this);
}

void UInventoryComponent::ServerHandleGameplayEvent_Implementation(FGameplayEventData Payload)
{
	HandleGameplayEventInternal(Payload);
}

void UInventoryComponent::HandleGameplayEventInternal(FGameplayEventData Payload)
{
	if (GetOwner()->HasAuthority())
	{
		FGameplayTag EventTag = Payload.EventTag;

		if (EventTag == UInventoryComponent::EquipItemActorTag)
		{
			if (const UAS_InventoryItemInstance* ItemInstance = Cast<UAS_InventoryItemInstance>(Payload.OptionalObject))
			{
				AddItemInstance(const_cast<UAS_InventoryItemInstance*>(ItemInstance));
				if (Payload.Instigator)
				{
					Cast<AActor>(Payload.Instigator)->Destroy();
				}
			}
		}
		else if (EventTag == UInventoryComponent::EquipNextTag)
		{
			EquipNext();
		}
		else if (EventTag == UInventoryComponent::DropItemTag)
		{
			DropItem();
		}
		else if (EventTag == UInventoryComponent::UnequipTag)
		{
			UnequipItem();
		}
	}
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const bool bShowDebug = CVarShowInventory.GetValueOnGameThread() != 0;
	if (bShowDebug)
	{
		for (const auto Item : InventoryList.GetItemsRef())
		{
			if (const UAS_InventoryItemInstance* ItemInstance = Item.InventoryItemInstance)
			{
				if (const UItemStaticData* ItemStaticData = ItemInstance->GetItemStaticData())
				{
					GEngine->AddOnScreenDebugMessage(
						-1,
						0,
						FColor::Blue,
						FString::Printf(TEXT("Item: %s"), *ItemStaticData->Name.ToString()));
				}
			}
		}
	}
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, InventoryList);
	DOREPLIFETIME(UInventoryComponent, CurrentItem);
}

