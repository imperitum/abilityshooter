// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/ItemActor.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "Components/SphereComponent.h"
#include "Engine/ActorChannel.h"
#include "Inventory/AS_InventoryItemInstance.h"
#include "Inventory/InventoryComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AItemActor::AItemActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	SetReplicateMovement(true);
	
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->SetupAttachment(RootComponent);
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AItemActor::OnActorOverlapped);
	SphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AItemActor::OnEquipped()
{
	ItemState = EItemState::Equipped;
	SetActorEnableCollision(false);
}

void AItemActor::OnUnequipped()
{
	ItemState = EItemState::None;
	SetActorEnableCollision(false);
}

void AItemActor::OnDropped()
{
	ItemState = EItemState::Dropped;
	
	GetRootComponent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);

	if (Owner)
	{
		const FVector Forward = Owner->GetActorForwardVector();

		const float ItemDropDistance = 100.f;
		const float TraceDistance = 1000.f;
		const float ItemSize = 10.f;

		const FVector TraceStart = Owner->GetActorLocation() + Forward * ItemDropDistance;
		const FVector TraceEnd = TraceStart - FVector::UpVector * TraceDistance;
		FVector TargetLocation = TraceStart;

		FHitResult TraceHit;
		if (UKismetSystemLibrary::LineTraceSingleByProfile(
			this,
			TraceStart,
			TraceEnd,
			TEXT("WorldStatic"),
			false,
			{ Owner },
			EDrawDebugTrace::None,
			TraceHit,
			true))
		{
			if (TraceHit.bBlockingHit)
			{
				TargetLocation = TraceHit.Location + FVector::UpVector * ItemSize;
			}
		}

		SetActorLocation(TargetLocation);
		SetActorEnableCollision(true);
	}
}

void AItemActor::Init(UAS_InventoryItemInstance* InInstance)
{
	ItemInstance = InInstance;
	InitInternal();
}

bool AItemActor::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	WroteSomething |= Channel->ReplicateSubobject(ItemInstance, *Bunch, *RepFlags);

	return WroteSomething;
}

// Called when the game starts or when spawned
void AItemActor::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority() && !ItemInstance && IsValid(ItemStaticDataClass))
	{
		ItemInstance = NewObject<UAS_InventoryItemInstance>();
		ItemInstance->Init(ItemStaticDataClass);
		SetActorEnableCollision(true);

		InitInternal();
	}
}

void AItemActor::InitInternal()
{
	
}

void AItemActor::OnRep_ItemState()
{
	if (ItemState == EItemState::Equipped)
	{
		SetActorEnableCollision(false);
	} else
		SetActorEnableCollision(true);
}

void AItemActor::OnRep_ItemInstance(UAS_InventoryItemInstance* OldItemInstance)
{
	if (IsValid(ItemInstance) && !IsValid(OldItemInstance))
	{
		InitInternal();
	}
}

void AItemActor::OnActorOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                   int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority())
	{
		FGameplayEventData Payload;
		Payload.Instigator = this;
		Payload.EventTag = UInventoryComponent::EquipItemActorTag;
		Payload.OptionalObject = ItemInstance;
	
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(OtherActor, UInventoryComponent::EquipItemActorTag, Payload);
	}
}

// Called every frame
void AItemActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AItemActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AItemActor, ItemInstance);
	DOREPLIFETIME(AItemActor, ItemState);
}

