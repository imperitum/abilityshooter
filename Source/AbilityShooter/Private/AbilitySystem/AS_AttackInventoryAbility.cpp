// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/AS_AttackInventoryAbility.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"

FGameplayEffectSpecHandle UAS_AttackInventoryAbility::GetWeaponEffectSpec(const FHitResult& Hit)
{
	if (UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponentFromActorInfo())
	{
		if (const UWeaponStaticData* WeaponStaticData = GetEquippedWeaponStaticData())
		{
			FGameplayEffectContextHandle ContextHandle = AbilitySystemComponent->MakeEffectContext();

			FGameplayEffectSpecHandle OutSpec = AbilitySystemComponent->MakeOutgoingSpec(WeaponStaticData->Effect, 1, ContextHandle);

			UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(OutSpec, FGameplayTag::RequestGameplayTag(TEXT("Attribute.Health")), -WeaponStaticData->Damage);

			return OutSpec;
		}
	}

	return FGameplayEffectSpecHandle();
}
