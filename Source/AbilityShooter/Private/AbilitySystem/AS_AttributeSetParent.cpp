// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/AS_AttributeSetParent.h"

#include "GameplayEffectExtension.h"
#include "GameplayEffectTypes.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Net/UnrealNetwork.h"


void UAS_AttributeSetParent::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.f, GetMaxHealth()));
	}
	if (Data.EvaluatedData.Attribute == GetMaxMovementSpeedAttribute())
	{
		const ACharacter* OwningCharacter = Cast<ACharacter>(GetOwningActor());
		UCharacterMovementComponent* MovementComponent = OwningCharacter->GetCharacterMovement();
		if (MovementComponent)
			MovementComponent->MaxWalkSpeed = GetMaxMovementSpeed();
	}
}

void UAS_AttributeSetParent::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UAS_AttributeSetParent, Health, OldHealth);
}

void UAS_AttributeSetParent::OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UAS_AttributeSetParent, MaxHealth, OldMaxHealth);
}

void UAS_AttributeSetParent::OnRep_Stamina(const FGameplayAttributeData& OldStamina)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UAS_AttributeSetParent, Stamina, OldStamina);
}

void UAS_AttributeSetParent::OnRep_MaxStamina(const FGameplayAttributeData& OldMaxStamina)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UAS_AttributeSetParent, MaxStamina, OldMaxStamina);
}

void UAS_AttributeSetParent::OnRep_MaxMovementSpeed(const FGameplayAttributeData& OldMovementSpeed)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UAS_AttributeSetParent, MaxMovementSpeed, OldMovementSpeed);
}

void UAS_AttributeSetParent::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UAS_AttributeSetParent, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UAS_AttributeSetParent, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UAS_AttributeSetParent, Stamina, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UAS_AttributeSetParent, MaxStamina, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UAS_AttributeSetParent, MaxMovementSpeed, COND_None, REPNOTIFY_Always);
}
