// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/AS_ParentGameplayAbility.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemLog.h"

void UAS_ParentGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                                const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                                const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	for (const auto GameplayEffect : EffectsToApplyOnStart)
	{
		ApplyGameplayEffect(ActorInfo, GameplayEffect);
	}
	
	if (IsInstantiated())
	{
		for (const auto GameplayEffect : EffectsToRemoveOnEnd)
		{
			EffectsHandlesToRemove.Add(ApplyGameplayEffect(ActorInfo, GameplayEffect));
		}
	}
}

void UAS_ParentGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	if (IsInstantiated())
	{
		for (const auto EffectToRemove : EffectsHandlesToRemove)
		{
			if (EffectToRemove.IsValid())
				ActorInfo->AbilitySystemComponent->RemoveActiveGameplayEffect(EffectToRemove);
		}

		EffectsHandlesToRemove.Empty();
	}
	
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

FActiveGameplayEffectHandle UAS_ParentGameplayAbility::ApplyGameplayEffect(const FGameplayAbilityActorInfo* ActorInfo, const TSubclassOf<UGameplayEffect> GameplayEffectToApply)
{
	if (GameplayEffectToApply.Get())
	{
		if (UAbilitySystemComponent* AbilitySystemComponent = ActorInfo->AbilitySystemComponent.Get())
		{
			const FGameplayEffectContextHandle GEContextHandle = AbilitySystemComponent->MakeEffectContext();

			const FGameplayEffectSpecHandle GESpecHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffectToApply, 1, GEContextHandle);

			if (GESpecHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*GESpecHandle.Data.Get(), AbilitySystemComponent);

				if (!ActiveGEHandle.WasSuccessfullyApplied())
				{
					ABILITY_LOG(Log, TEXT("Ability %s failed to apply effect %s"), *GetName(), *GetNameSafe(GameplayEffectToApply));
				}
				else
				{
					return ActiveGEHandle;
				}
			}
		}
	}
	
	return {};
}

AAbilityShooterCharacter* UAS_ParentGameplayAbility::GetAbilityShooterCharacterFromActorInfo() const
{
	return Cast<AAbilityShooterCharacter>(GetAvatarActorFromActorInfo());
}
