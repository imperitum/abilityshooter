// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/AS_InventoryAbility.h"

#include "Inventory/AS_InventoryItemInstance.h"
#include "Inventory/InventoryComponent.h"
#include "Inventory/WeaponItemActor.h"

void UAS_InventoryAbility::OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnGiveAbility(ActorInfo, Spec);

	InventoryComponent = ActorInfo->OwnerActor.Get()->FindComponentByClass<UInventoryComponent>();
}

UInventoryComponent* UAS_InventoryAbility::GetInventoryComponent() const
{
	return InventoryComponent;
}

const UAS_InventoryItemInstance* UAS_InventoryAbility::GetEquippedItemInstance() const
{
	return InventoryComponent ? InventoryComponent->GetCurrentItem() : nullptr;
}

const UItemStaticData* UAS_InventoryAbility::GetEquippedItemStaticData() const
{
	const UAS_InventoryItemInstance* InventoryItemInstance = GetEquippedItemInstance();
	
	return InventoryItemInstance ? InventoryItemInstance->GetItemStaticData() : nullptr;
}

const UWeaponStaticData* UAS_InventoryAbility::GetEquippedWeaponStaticData() const
{
	return Cast<UWeaponStaticData>(GetEquippedItemStaticData());
}

AItemActor* UAS_InventoryAbility::GetEquippedItemActor() const
{
	const UAS_InventoryItemInstance* EquippedItem = GetEquippedItemInstance();

	return EquippedItem ? EquippedItem->GetItemActor() : nullptr;
}

AWeaponItemActor* UAS_InventoryAbility::GetEquippedWeaponActor() const
{
	return Cast<AWeaponItemActor>(GetEquippedItemActor());
}
