// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/AbilitySystemPhysicsVolume.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"

AAbilitySystemPhysicsVolume::AAbilitySystemPhysicsVolume()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AAbilitySystemPhysicsVolume::ActorEnteredVolume(AActor* Other)
{
	Super::ActorEnteredVolume(Other);

	if (!HasAuthority()) return;

	if (UAbilitySystemComponent* AbilitySystemComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Other))
	{
		EnteredActorsInfoMap.Add(Other);
		
		for (auto Effect : OngoingEffectsToApply)
		{
			FGameplayEffectContextHandle EffectContextHandle = AbilitySystemComponent->MakeEffectContext();
			EffectContextHandle.AddInstigator(Other, this);

			FGameplayEffectSpecHandle EffectSpecHandle = AbilitySystemComponent->MakeOutgoingSpec(Effect, 0, EffectContextHandle);

			if (EffectSpecHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*EffectSpecHandle.Data.Get());
				if (ActiveGEHandle.WasSuccessfullyApplied())
				{
					EnteredActorsInfoMap[Other].AppliedEffects.Add(ActiveGEHandle);
				}
			}
		}

		for (auto Ability : OngoingAbilitiesToGive)
		{
			FGameplayAbilitySpecHandle AbilitySpecHandle = AbilitySystemComponent->GiveAbility(Ability);
			EnteredActorsInfoMap[Other].AppliedAbilities.Add(AbilitySpecHandle);
		}

		for (auto Ability : PermanentAbilitiesToGive)
		{
			AbilitySystemComponent->GiveAbility(Ability);
		}

		for (auto EventTag : EventsToSendOnEnter)
		{
			FGameplayEventData EventData;
			EventData.EventTag = EventTag;

			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Other, EventTag, EventData);
		}
	}
}

void AAbilitySystemPhysicsVolume::ActorLeavingVolume(AActor* Other)
{
	Super::ActorLeavingVolume(Other);

	if (!HasAuthority()) return;

	if (UAbilitySystemComponent* AbilitySystemComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Other))
	{
		if (!EnteredActorsInfoMap.Find(Other)) return;
		
		for (auto ActiveGameplayEffect : EnteredActorsInfoMap[Other].AppliedEffects)
		{
			AbilitySystemComponent->RemoveActiveGameplayEffect(ActiveGameplayEffect);
		}

		for (auto Effect : OnExitEffectsToApply)
		{
			FGameplayEffectContextHandle EffectContextHandle = AbilitySystemComponent->MakeEffectContext();
			EffectContextHandle.AddInstigator(Other, this);

			FGameplayEffectSpecHandle EffectSpecHandle = AbilitySystemComponent->MakeOutgoingSpec(Effect, 0, EffectContextHandle);

			if (EffectSpecHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*EffectSpecHandle.Data.Get());
			}
		}

		for (auto ActiveAbility : EnteredActorsInfoMap[Other].AppliedAbilities)
		{
			AbilitySystemComponent->ClearAbility(ActiveAbility);
		}

		for (auto EventTag : EventsToSendOnExit)
		{
			FGameplayEventData EventData;
			EventData.EventTag = EventTag;

			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Other, EventTag, EventData);
		}

		EnteredActorsInfoMap.Remove(Other);
	}
}

void AAbilitySystemPhysicsVolume::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bDrawDebug)
	{
		DrawDebugBox(GetWorld(), GetActorLocation(), GetBounds().BoxExtent, FColor::Green, false, 0, 0, 5);
	}
}
