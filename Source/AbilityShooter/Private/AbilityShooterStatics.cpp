// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityShooterStatics.h"


const UItemStaticData* UAbilityShooterStatics::GetItemStaticData(TSubclassOf<UItemStaticData> ItemDataClass)
{
	if (IsValid(ItemDataClass))
	{
		return GetDefault<UItemStaticData>(ItemDataClass);
	}

	return nullptr;
}
