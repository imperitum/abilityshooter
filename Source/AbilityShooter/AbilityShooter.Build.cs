// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class AbilityShooter : ModuleRules
{
	public AbilityShooter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput", "NetCore" });
		
		PublicIncludePaths.Add("AbilityShooter/");
		
		PrivateDependencyModuleNames.AddRange(new string[] {
			"GameplayAbilities",
				"GameplayTags",
					"GameplayTasks"
		});
	}
}
